from formencode import Schema, validators

class RegisterForm(Schema):
    name = validators.UnicodeString(not_empty=True, max=45)
    surname = validators.UnicodeString(not_empty=True, max=45)
    email = validators.Email(not_empty=True, max=45)
    password = validators.UnicodeString(not_empty=True, max=45)

class LoginForm(Schema):
    email = validators.Email(not_empty=True, max=45)
    password = validators.UnicodeString(not_empty=True, max=45)
