from hashlib import sha512 
from pyramid.view import view_config
from pyramid.response import Response
from pyramid_simpleform import Form
from pyramid_simpleform.renderers import FormRenderer
from pymongo import MongoClient
from forms import RegisterForm, LoginForm

import random
import re
import pyramid.httpexceptions as exc



class LotteryViews(object):
    def __init__(self, request):
        self.request = request
        self.db = MongoClient('mongodb://localhost:27017/')
        self.user = self.db.lottery.user
        self.localizer = request.localizer

    @view_config(route_name='home', renderer='templates/lotterytemplate.pt')
    def home_view(self):
        """The home view"""
        logged_in = 'uid' in self.request.session
        if logged_in:
               return {'action' : 'loggedin'} 
        form = Form(self.request, schema=RegisterForm)
        return {'form': FormRenderer(form), 'action' : 'register'}


    @view_config(route_name='register', renderer='templates/lotterytemplate.pt')
    def register_action(self):
        """Register the user, first validate the input """
        logged_in = 'uid' in self.request.session        
        if logged_in:
            raise exc.HTTPFound(self.request.route_url("home"))
        #Create the form object    
        form = Form(self.request, schema=RegisterForm)
        #Validate the form
        if form.validate(): 
            user = form.data
            exist = self.user.find_one({'email' : re.compile(user['email'], re.IGNORECASE)})
            #Does the email already exist?
            if exist:
                form.errors['email'] = 'Email is already in use'
                return {'form': FormRenderer(form), 'action' : 'register'}
            else:            
                user['password'] = sha512(user['password']).hexdigest()
                #Save the new user
                self.request.session['uid'] = self.user.insert(user)
                #redirect
                raise exc.HTTPFound(self.request.route_url("home"))

        return {'form': FormRenderer(form), 'action' : 'register'}

    @view_config(route_name='login', renderer='templates/lotterytemplate.pt')
    def login_view(self):
        """Try to authenticate the user"""
        logged_in = 'uid' in self.request.session
        if logged_in:
            raise exc.HTTPFound(self.request.route_url("home"))

        form = Form(self.request, schema=LoginForm)
        if form.validate():
            user = form.data
            exist = self.user.find_one(
                       {'email' : re.compile(user['email'],
                        re.IGNORECASE),
                         'password' : sha512(user['password']).hexdigest()})
            #are we a valid login?
            if exist:
                self.request.session['uid'] = exist['_id'];
                raise exc.HTTPFound(self.request.route_url("home"))
            else:
                form.errors['email'] = 'Invalid credentials'

        return {'form': FormRenderer(form), 'action' : 'login'}

    @view_config(route_name='draw', renderer='templates/lotterytemplate.pt')
    def draw_view(self):
        users = self.user.find()
        num_users = users.count()
        #pick a random user from the user collection
        try:
            winner = users[random.randint(1, num_users)] 
        except IndexError:
            #The cursor is being lazy like hell
            raise exc.HTTPFound(self.request.route_url("draw"))
        #give it back to the user
        return {'action' : 'draw', 'winner' : winner}